<?php

/**

 * Created by PhpStorm.

 * User: HanhDo

 * Date: 4/9/2019

 * Time: 3:05 PM

 */

$sc = STInput::get('sc');

?>

<div id="availablility_tab" class="partner-availability">

    <?php

    switch ($sc){

        case 'edit-tours':

            ?>

            <div class="row">

                <div class="col-md-12">

                    <?php echo st()->load_template('availability/form-tour'); ?>

                </div>

            </div>

            <?php

            break;

        case 'edit-activity':

            ?>

            <div class="row">

                <div class="col-md-12">

                    <?php echo st()->load_template('availability/form-activity'); ?>

                </div>

            </div>

            <?php

            break;

        case 'edit-flight':

            echo st()->load_template('availability/form-flight');

            break;

        default:

            ?>

            <div class="row">

                <div class="col-md-12">

                    <?php echo st()->load_template('availability/form'); ?>

                </div>

            </div>

            <?php

            break;

    }

    ?>

</div>

