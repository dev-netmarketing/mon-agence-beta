<?php

    /**

     * Created by PhpStorm.

     * User: Administrator

     * Date: 16-11-2018

     * Time: 11:29 AM

     * Since: 1.0.0

     * Updated: 1.0.0

     */



    $room_id    = get_the_ID();

    $room_id    = TravelHelper::post_translated($room_id);

    $room_num   = $data['i'];



	if($data['post_id'] != NULL){

		$item_id = $data['hotel_id'];

	}else{

		$item_id = STInput::request( 'room_parent' );

    }

    

    $result                 = $data['result'];

    $ranges_final           = $data['ranges_final'][$room_num];

    $shared_arrangements    = $data['shared_arrangements'];

    $shared_extras          = $data['shared_extras'];

    $period_ids_arr         = $data['period_ids_arr'];



    $period                 = STDate::dateDiff($today, $check_in);

    

    $prix_single_arr        = $data['prix_single_arr'][$room_num];

    $single_person          = $data['single_person'][$room_num]['single_person'];



    $prix_bed_discount_arr  = $data['prix_bed_discount_arr'][$room_num];

    $discount_on_beds       = $data['single_person'][$room_num]['discount_beds'];



    $extra_supp             = $data['ranges_final'][$room_num];

    // Main function used to calculate prices/reduction/marge/.. on each periodes

    $arrangement_list       = STHotel::calcul_price_arrangement($ranges_final, $shared_arrangements);

    $extra_supp             = STHotel::get_supp_sum($extra_supp, $shared_extras);

    //echo json_encode($period_ids_arr);

    

?>



<div class="item single-room" id="room-<?php echo $room_num ?>">

    <input type="hidden" name="period_ids_arr" value="<?php echo base64_encode(json_encode($period_ids_arr)) ?>">

    <input type="hidden" name="retrocession" value="<?php echo base64_encode(json_encode($data['retrocession'])) ?>">

    <input type="hidden" name="periode_id" value="<?php echo $ranges_final->id ?>">

    <input type="hidden" name="item_id" value="<?php echo $item_id ?>">

    <input type="hidden" name="<?php echo "room-" . $room_num ?>" value="<?php echo $data['post_id'] ?>">

    <input type="hidden" name="room-number" value="<?php echo $room_num ?>">

    <input type="hidden" name="room_num_search" value="<?php echo $data['num'] ?>" >

    <input type="hidden" name="check_in" value="<?php echo STInput::request( 'start' ); ?>"/>

    <input type="hidden" name="check_out" value="<?php echo STInput::request( 'end' ); ?>"/>

    <input type="hidden" name="start" value="<?php echo STInput::request( 'start' ); ?>"/>

    <input type="hidden" name="end" value="<?php echo STInput::request( 'end' ); ?>"/>

    <input type="hidden" name="is_search_room" value="<?php echo STInput::request( 'is_search_room' ); ?>">

    <input type="hidden" name="<?php echo "result-room-".$room_num ?>" value="<?php echo base64_encode(json_encode($result)); ?>">

    <input type="hidden" name="<?php echo "data_room-room-".$room_num ?>" value="<?php echo base64_encode(json_encode($data_room)); ?>">



    <div class="row">

        <div class="col-xs-12 col-md-12">

            <div class="row">

                <div class="col-xs-3 col-md-3">

                    <h2 class="heading "><?php echo get_the_title($room_id);?> </h2>

                </div>

                    <?php

                        $room_test      = array();

                        $start          = TravelHelper::convertDateFormat( STInput::request( 'start' ) );														

                        $end            = TravelHelper::convertDateFormat( STInput::request( 'end' ) );

                        $numberday      = STDate::dateDiff( $start, $end );

                        $total_price_arr = [];

                        $total_pax      = 0;

                        $today          = strtotime( date( 'Y-m-d' ) );

                        $select_arrangement = '<select id="arrangement-room-'.$room_num.'" name="arrangement-room-'.$room_num.'" class="form-control" onchange="get_arrang_price('.$data['i'].');">';

                        $select_arrangement .= '<option value="-1" selected>--- Choisissez votre arrangement ---</option>';

                        if ( $data['pricing'] ) {



                            $initial_price      = 0;

                            $total_price        = 0;

                            $input_total_price  = 0;

                            $min_price          = min($arrangement_list);

                            $price_output   = '<span class="price" id=price-room-'.$room_num .'>' . TravelHelper::format_money( round( $min_price ) ) .'</span>';

                            $price_output   .= '<input type="hidden" name="sale-price-'.$room_num .'" id="sale-price-'.$room_num .'" value="'. round( $min_price ) .'">';

                            foreach ($arrangement_list as $arrangement_id => $arrangement_price) {



                                $arrangement = get_term_by('term_id', $arrangement_id, 'hotel-accommodation');

                                //$price_hidden   .= '<input type="hidden" id="old-price-'.$arrangement_id.'-room-'.$data['i'].'" value="'.$arrangement_price.'">';

                                $price_hidden   .= '<input type="hidden" id="price-'.$arrangement_id.'-room-'.$room_num.'" value="'.round( $arrangement_price ).'">';



                                if( round( $min_price ) == round( $arrangement_price ) ){

                                    $select_arrangement .= '<option value="'.$arrangement_id.'" selected>'. $arrangement->name .'</option>';

                                } else {

                                    $select_arrangement .= '<option value="'.$arrangement_id.'">'. $arrangement->name .'</option>';

                                }

                            }

                        }

                        $select_arrangement .= '</select>';                                   

                        ?>

                        <!----- Arrangement! ----->

                        <div class="col-xs-12 col-md-6">

                            <div class="widgets">

                                <h5>Avec arrangement :</h5>

                                <?php echo $select_arrangement; ?>

                            </div> 

                        </div>

                         <!----- Prices! ----->

                        <div class="heading col-xs-12 col-md-3">

                            <div class="price-wrapper">

                            <!----- Discount! ----->

                            <?php if($discount): ?>



                            <?php endif; ?>

                               

                                <?php echo $price_output; ?>

                                <?php echo $price_hidden; ?>

                                

                                <span class="unit">

                                    <?php

                                    if ( isset( $price_by_per_person ) && $price_by_per_person == 'on' ) {

                                        // echo sprintf( _n( '/person', '/%d persons', $total_person, ST_TEXTDOMAIN ), $total_person );

                                        echo sprintf( _n( '/nuit', '/%d nuits', $numberday, ST_TEXTDOMAIN ), $numberday );

                                    } else {

                                        echo sprintf( _n( '/nuit', '/%s nuits', $numberday ), $numberday );

                                    }

                                    ?>

                                </span>

                            </div>

                        </div>

                         <!----- Extras! ----->

                        <?php 

                        if( !empty($shared_extras) ):

                        ?>

                        <div class="col-xs-12 col-md-12">

                            <div class="widgets">

                                <h5>Supplément(s) : <a onclick="reset_extra(<?php echo $room_num ?>)" class="reset">effacer</a></h5>

                                <?php

                                foreach ($shared_extras as $extra) {

                                    $extra = get_term_by('term_id', $extra, 'extra');

                                ?>

                                <div class="form-check supp-room-radio">

                                    <input  class="form-check-input" 

                                            type="radio" 

                                            name="<?php  echo "extra-room-".$room_num ?>" 

                                            id="<?php echo "extra-id-".$extra->term_id ."-room-".$room_num ?>" 

                                            value="<?php echo $extra->term_id ?>">

                                    

                                    <label  class="form-check-label" 

                                            for="<?php echo "extra-id-".$extra->term_id."-room-".$room_num ?>"> 

                                            <?php echo "Supplément " . $extra->name . ": <b>" . round( $extra_supp[$extra->term_id] ). " DT</b> (par chambre)" ?>

                                    </label>

                                    

                                </div>

                                <input  type="hidden" 

                                        name="extra-id-<?php echo $extra->term_id . "-room-" . $room_num;?>[price]"

                                        value="<?php echo round( $extra_supp[$extra->term_id] );?>">

                                <input  type="hidden" 

                                        name="extra-id-<?php echo $extra->term_id . "-room-" . $room_num;?>[title]"

                                        value="<?php echo $extra->name; ?>">        

                                <?php } // end for ?>

                            </div> 

                        </div>

                        <?php endif; ?>

                        <!----- Supp Single !----->

                        <?php 

                        if( $single_person ):

                            $supp_single['id_extra']    = "99";

                            $supp_single['name_extra']  = "Supplément Single";

                            $supp_single['prix']        = array_sum($prix_single_arr) * $numberday ;

                        ?>

                        <div class="col-xs-12 col-md-12">

                            <div class="widgets">

                                <h5>Autres : </h5>

                                <div class="form-check supp-room-radio def-supp">

                                    <input  class="form-check-input" 

                                            type="radio"

                                            checked = "true"

                                            name="<?php echo "def-extra-room-".$room_num ?>" 

                                            id="<?php echo "def-extra-id-".$supp_single['id_extra'] ."-room-".$room_num;?>" 

                                            value="<?php echo $supp_single['id_extra'] ?>">

                                    <label  class="form-check-label" 

                                            for="<?php echo "def-extra-id-".$supp_single['id_extra']."-room-".$room_num;?>"> 

                                            <?php echo $supp_single['name_extra'] . ": ".round( $supp_single['prix'] )." DT (* OBLIGATOIRE) " ?>

                                    </label>

                                </div>

                                <input  type="hidden" 

                                        name="def-extra-id-<?php echo $supp_single['id_extra'] . "-room-" . $room_num;?>[price]"

                                        value="<?php echo round( $supp_single['prix'] ) ?>">

                                <input  type="hidden" 

                                        name="def-extra-id-<?php echo $supp_single['id_extra'] . "-room-" . $room_num;?>[title]"

                                        value="<?php echo $supp_single['name_extra']; ?>">

                            </div>

                        </div>

                        <?php endif; ?>

                        <!----- Supp 3/4 lits !----->

                        <?php 

                        if( $discount_on_beds ):

                            $bed_discount['id_extra']    = "999";

                            $bed_discount['name_extra']  = "Réduction 3éme/4éme lits";

                            $bed_discount['prix']        = array_sum($prix_bed_discount_arr) ;

                        ?>

                        <div class="col-xs-12 col-md-12">

                            <div class="widgets">

                                <h5>Réductions : </h5>

                                <div class="form-check supp-room-radio def-supp">

                                    <input  class="form-check-input" 

                                            type="radio"

                                            checked = "true"

                                            name="<?php echo "def-bed-discount-room-".$room_num ?>" 

                                            id="<?php echo "def-bed-discount-id-".$bed_discount['id_extra'] ."-room-".$room_num ?>" 

                                            value="<?php echo $bed_discount['id_extra'] ?>">

                                    <label  class="form-check-label" 

                                            for="<?php echo "def-bed-discount-id-".$bed_discount['id_extra']."-room-".$room_num ?>"> 

                                            <?php echo $bed_discount['name_extra'] . ": ".$bed_discount['prix']." % (* OBLIGATOIRE) " ?>

                                    </label>

                                </div>

                                <input  type="hidden" 

                                        name="def-bed-discount-id-<?php echo $bed_discount['id_extra'] ."-room-".$room_num; ?>[price]"

                                        value="<?php echo $bed_discount['prix'] ?>">

                                <input  type="hidden" 

                                        name="def-bed-discount-id-<?php echo $bed_discount['id_extra'] ."-room-".$room_num; ?>[title]"

                                        value="<?php echo $bed_discount['name_extra']; ?>">

                            </div>

                        </div>

                        <?php endif; ?>

                </div>

            </div>

        </div>

        <script type="text/javascript">

            function get_arrang_price(i){

                var arrang_id = jQuery("#arrangement-room-"+i).val();

                console.log('arrang_id', arrang_id);



                var new_price = jQuery("#price-"+arrang_id+"-room-"+i).val();

                console.log('new_price', new_price);



                var old_price = jQuery("#old-price-"+arrang_id+"-room-"+i).val();

                if(new_price > 0) {

                    jQuery("#price-room-"+i).html(new_price + " DT");

                    jQuery("#old-price-room-"+i).html(old_price + " DT");

                    jQuery("#sale-price-"+i).val(new_price);

                }

            }

            

            function reset_extra(i){

                if(i > 0) {

                    jQuery('input[name=extra-room-'+i+']').removeAttr('checked');

                }

            }

        </script>

    </div>

    

