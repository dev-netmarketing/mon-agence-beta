<?php
    /**
     * Created by PhpStorm.
     * User: Administrator
     * Date: 14-11-2018
     * Time: 8:16 AM
     * Since: 1.0.0
     * Updated: 1.0.0
     */
    $post_id      = get_the_ID();
    $post_translated = TravelHelper::post_translated($post_id);
    $thumbnail_id = get_post_thumbnail_id($post_translated);
    $hotel_star   = (int)get_post_meta( $post_translated, 'hotel_star', true );
    $address      = get_post_meta( $post_translated, 'address', true );
    $review_rate  = STReview::get_avg_rate();
    $price        = STHotel::get_hotel_price($post_id);
    $today        = strtotime( date( 'Y-m-d' ) );
    $discount     = STHotel::get_discount_by_period($post_id, $today);
    $msg_promotion = STHotel::get_msg_promotion($post_id, $today);
	//$price = get_post_meta( $post_id, 'min_price', true );
    $count_review = get_comment_count($post_translated)['approved'];
    $class='col-xs-12 col-sm6 col-md-3';
if(isset($slider) and $slider)
    $class = '';
?>
<div class="<?php echo esc_attr($class); ?>">
    <div class="item has-matchHeight">
        <div class="featured-image">
        <?php if($discount) { ?>
            <span class="st_sale_class box_sale sale_small"><?php echo $discount."%"; ?></span>
        <?php } // end discount ?>
        <?php if($msg_promotion) { ?>
            <div class="service-tag bestseller msg_promotion">
                <div class="feature_class st_featured featured"><?php echo $msg_promotion; ?></div>
            </div>
        <?php } // end msg_promotion ?>
        <?php
                $is_featured = get_post_meta( $post_translated, 'is_featured', true );
                if ( $is_featured == 'on' ) {
                    ?>
                    <div class="featured"><?php echo esc_html__( 'Bestseller', ST_TEXTDOMAIN ) ?></div>
                    <?php
                }
            ?>
            <a href="<?php echo get_the_permalink($post_translated); ?>">
                <img src="<?php echo wp_get_attachment_image_url( $thumbnail_id, array(450, 417) ); ?>" alt=""
                     class="img-responsive img-full">
            </a>
            <?php echo st()->load_template( 'layouts/modern/common/star', '', [ 'star' => $hotel_star ] ); ?>
        </div>
        <h4 class="title"><a href="<?php echo get_the_permalink($post_translated) ?>" class="st-link c-main"><?php echo get_the_title($post_translated) ?></a></h4>
        <?php
            if ( $address ) {
                ?>
                <div class="sub-title"><?php echo TravelHelper::getNewIcon('Ico_maps', '#666666', '15px', '15px', true); ?><?php echo esc_html( $address ); ?></div>
                <?php
            }
        ?>
        <div class="section-footer">
            <?php if($discount) : ?>
                <div class="price-wrapper">
                    <?php echo TravelHelper::getNewIcon('thunder', '#ffab53', '10px', '16px'); ?>
                        <div class="final-price">
                        <?php echo __('À partir de', ST_TEXTDOMAIN); ?> <span class="price"><?php echo TravelHelper::format_money( STHotel::discount_calculation($price, $discount) ) ?></span><span
                        class="unit"><?php echo __( '/nuit', ST_TEXTDOMAIN ) ?></span>
                        </div>
                        <div class="old-price">
                        <?php echo __('Au lieu', ST_TEXTDOMAIN); ?> <span class="price"><?php echo TravelHelper::format_money( $price ) ?></span>
                        </div>
                </div>
            <?php else: ?>
                <div class="price-wrapper price-min-wt">
                    <?php echo TravelHelper::getNewIcon('thunder', '#ffab53', '10px', '16px'); ?>
                    <?php echo __('À partir de', ST_TEXTDOMAIN); ?> <span class="price"><?php echo TravelHelper::format_money( $price ) ?></span><span
                        class="unit"><?php echo __( '/nuit', ST_TEXTDOMAIN ) ?></span>
                </div>
            <?php endif; ?>
        </div>
    </div>
</div>
