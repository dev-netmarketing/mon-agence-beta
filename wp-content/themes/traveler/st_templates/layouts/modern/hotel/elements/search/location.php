<?php
    $enable_tree = st()->get_option( 'bc_show_location_tree', 'off' );
    $location_id = STInput::get( 'location_id', '' );
    $location_name = STInput::get( 'location_name', '' );
    if(empty($location_name)){
        if(!empty($location_id)){
            $location_name = get_the_title($location_id);
        }
    }
    if ( $enable_tree == 'on' ) {
        $lists     = TravelHelper::getListFullNameLocation( 'st_hotel' );
        $locations = TravelHelper::buildTreeHasSort( $lists );
    } else {
        $locations = TravelHelper::getListFullNameLocation( 'st_hotel' );
    }
    $locations_ = TravelHelper::getListFullNameLocation( 'st_hotel' );
    $has_icon = ( isset( $has_icon ) ) ? $has_icon : false;
    if(is_singular('location')){
        $location_id = get_the_ID();
    }
    $hotels = STHotel::get_all_hotels();
?>
<div class="form-group form-extra-field dropdown clearfix field-detination <?php if ( $has_icon ) echo 'has-icon' ?>">
    <?php
        if ( $has_icon ) {
            echo TravelHelper::getNewIcon('ico_maps_search_box');
        }
    ?>
    <div class="dropdown" data-toggle="--dropdown" id="dropdown--destination">
    <label><?php echo __( 'Destinations', ST_TEXTDOMAIN ); ?></label>
        <div class="render">
            <span class="destination">                
            </span>
            <?php
            if(empty($location_name)) {
                $destination = __('Où allez-vous ?', ST_TEXTDOMAIN);
            }else{
                $destination = esc_html($location_name);
            }
            ?>
        <select name="location_search" id="location_search"  class="form-control locations_select">
            <?php
            $selected = '';
            if ( is_array( $locations_ ) && count( $locations_ ) ):
                echo '<optgroup label="Destinations">';
                foreach ( $locations_ as $key => $value ):
                    ($value->ID == $location_id ? $selected = 'selected' : $selected = '');
                    ?>
                    <option <?=$selected?> class="item location_item" data-id="<?php echo $value->ID; ?>" value="<?php echo esc_attr($value->post_title); ?>">
                        <span><?php echo esc_attr($value->post_title); ?></span></option>
                <?php
                endforeach;
                echo '</optgroup>';
            endif;
            if ( is_array( $hotels ) && count( $hotels ) ):
                echo '<optgroup label="Hôtels">';
                foreach ( $hotels as $hotel ):
                    ?>
                    <option class="item hotel_item" 
                            data-id="<?php echo $hotel['ID']; ?>" 
                            data-url="<?php echo get_permalink($hotel['ID']); ?>" 
                            value="<?php echo $hotel['post_title'] ?>">
                        <span><?php  echo $hotel['post_title'] ?></span></option>
                <?php
                endforeach;
                echo '</optgroup>';
            endif;
            ?>
        </select>
        </div>
        <input type="hidden" name="location_name" id="location_name" value="<?php echo esc_attr($location_name); ?>"/>
        <input type="hidden" name="location_id" id="location_id" value="<?php echo esc_attr($location_id); ?>"/>
    </div>
    <ul class="dropdown-menu" aria-labelledby="dropdown-destination">
        <?php
            if ( $enable_tree == 'on' ) {
                New_Layout_Helper::buildTreeOptionLocation( $locations, $location_id );
            } else {
                if ( is_array( $locations ) && count( $locations ) ):
                    foreach ( $locations as $key => $value ):
                        ?>
                        <li class="item" data-value="<?php echo esc_attr($value->ID); ?>">
                            <?php echo TravelHelper::getNewIcon('ico_maps_search_box'); ?>
                            <span><?php echo esc_attr($value->fullname); ?></span></li>
                    <?php
                    endforeach;
                endif;
            }
        ?>
    </ul>
</div>
<script type="text/javascript">
jQuery(document).ready(function($) {
    $('.locations_select').select2({width: "100%",allowClear: false});
    $('.btn-search').click(function() {
        $('.map-content-loading').fadeIn();
    });
    $(".locations_select").on("select2:select", function (e) { 
        var location    = $(e.currentTarget).val();
        var location_id = $(e.currentTarget).find(":selected").data("id");
        var hotel_url = $(e.currentTarget).find(":selected").data("url");
        $("#location_name").val(location);
        $("#location_id").val(location_id);
        console.log('location_id', location_id); 
        console.log('location_name', location);
        console.log('hotel_url', hotel_url);
        if(hotel_url){
            $('.map-content-loading').fadeIn();
            window.location.replace(hotel_url);
            
        }
    });
});
</script>