<?php
$adult_number    = STInput::get( 'adult_number1','');
$child_number    = STInput::get( 'child_number', 0 );
$result =  STInput::request('result', 0);
$room_num = $result['room_num_search']	;
for ($i = 1; $i <= $room_num; $i++) { 
$resultRoom = array();
$max_size = $result['adult_number'.$i] +  $result['child_number'.$i];
global $varmaxsize;
$varmaxsize=$max_size; 
$adult_number = 'adult_number'.$i;
$child_number = 'child_number'.$i;
$resultRoom['room_num_search']= $result['room_num_search'];
$resultRoom[$adult_number] = $result['adult_number'.$i];
$resultRoom[$child_number] = $result['child_number'.$i];
if($result['child_number'.$i]){
for ($j = 1; $j <= $result['child_number'.$i]; $j++) {
$age_child = 'age_number_'.$i.'_'.$j;
$resultRoom[$age_child] = $result['age_number_'.$i.'_'.$j]; 
}	
}
}
 ?>
<div class="form-group form-extra-field dropdown clearfix field-guest <?php if ( $has_icon ) echo ' has-icon '; ?>">
	<?php
        if ( $has_icon ) {
            echo TravelHelper::getNewIcon( 'ico_guest_search_box' );
        }
    ?>
	<ul class="guest-menu">
		<div class="repartition_chambre" style="display: block;" id="select_0">
			<?php for ($c = 1; $c <= 5; $c++) { ?>
			<div class="repartition_une_chambre col-md-12"
				<?php	if($c <= $result["room_num_search"] || $c == 1) { echo'style="display: block;"'; } else { echo'style="display: none;"' ; } ?>
				id="select_<?php echo $c ;?>">
				<div> <label><strong><?php echo esc_html__( 'Chambre' , ST_TEXTDOMAIN ) ?>
							<?php echo $c ; ?></strong></label></div>
				<li class="item">
					<label><?php echo esc_html__( 'Nombre des adultes', ST_TEXTDOMAIN ) ?></label>
					<div class="select-wrapper" style="width: 50px;">
						<select name="adult_number<?php echo $c ; ?>" class="form-control app"
							id="adult_number<?php echo $c ; ?>">
							<?php	for ($counta = 0; $counta <= 6; $counta++) { ?>
							<option value="<?php echo esc_attr($counta); ?>"
								<?php if($result['adult_number'.$c] == $counta || $counta == 2){echo'selected';} ?>>
								<?php echo esc_attr($counta); ?></option>
							<?php } ?>
						</select>
					</div>
				</li>
				<li class="item">
					<label><?php echo esc_html__( 'Nombre des enfants', ST_TEXTDOMAIN ) ?></label>
					<div class="select-wrapper" style="width: 50px;">
						<select name="child_number<?php echo $c ; ?>" class="form-control app"
							onchange="afficher_masquer_age_enf(this.value,<?php echo $c ; ?>)"
							id="child_number<?php echo $c ; ?>">
							<?php	for ($countc = 0; $countc <= 3; $countc++) { ?>
							<option value="<?php echo esc_attr($countc); ?>"
								<?php if($result["child_number".$c.""] == $countc){echo'selected';} ?>>
								<?php echo esc_attr($countc); ?></option>
							<?php } ?>
						</select>
					</div>
				</li>
				<div class="col-md-12">
					<?php for ($countag = 1; $countag <= 3; $countag++) { ?>
					<li class="item col-md-4">
						<div id="agenf_<?php echo $c ; ?>_<?php echo $countag ; ?>" class="agenf"
							<?php	if($countag <= $result["child_number".$c.""]) { echo'style="display: block;"'; } else { echo'style="display: none;"' ; } ?>">
							<label><?php echo esc_html__( 'Âge', ST_TEXTDOMAIN ) ?></label>
							<div class="select-wrapper" style="width: 50px;">
								<select name="age_number_<?php echo $c ; ?>_<?php echo $countag ; ?>"
									class="form-control app age_number"
									id="age_number_<?php echo $c ; ?>_<?php echo $countag ; ?>">
									<?php	for ($j = 1; $j <= 15; $j++) { ?>
									<option value="<?php echo esc_attr($j); ?>"
										<?php if($result["age_number_".$c."_".$countag.""] == $j){echo'selected';} ?>>
										<?php echo esc_attr($j); ?></option>
									<?php } ?>
								</select>
								<input type="hidden" name="all_ages[]" value="" />
							</div>
						</div>
					</li>
					<?php } ?>
				</div>
			</div>
			<?php } ?>
		</div>
		<span
			class="hidden-lg hidden-md hidden-sm btn-close-guest-form"><?php echo __('Close', ST_TEXTDOMAIN); ?></span>
	</ul>
	<!--<i class="fa fa-angle-down arrow"></i>-->
</div>
<script>
	function afficher_masquer_select_ch(e) {
		if (0 != e) {
			for (var t = 1; t <= e; t++)
				document.getElementById("select_" + t).style.display = "";
			for (var n = t; n <= 5; n++)
				document.getElementById("select_" + n).style.display = "none";
			document.getElementById("select_0").style.display = ""
		} else
			document.getElementById("select_0").style.display = "none"
	}
	function afficher_masquer_age_enf(e, t) {
		for (var n = 1; n <= 3; n++)
			n <= e ? document.getElementById("agenf_" + t + "_" + n).style.display = "block" : (document.getElementById(
					"agenf_" + t + "_" + n).style.display = "none",
				document.getElementById("agenf_" + t + "_" + n).value = "")
	}
	<?php 
		if($_GET["room_num_search"])
		{
			$x=$_GET["room_num_search"];
			for($i=1;$i<=$x;$i++){
				echo 'document.getElementById("select_'.$i.'").style.display = "";';
				if($_GET["adult_number$i"])
				{ 
					echo 'document.getElementById("adult_number'.$i.'").value = '.$_GET["adult_number$i"].';';
				}
				if( $_GET["adult_number$i"] == 0)
				{ 
					echo 'document.getElementById("adult_number'.$i.'").value = 0;';
				}
				if($_GET["child_number$i"])
				{ 
					$y=$_GET["child_number$i"];
					echo 'document.getElementById("child_number'.$i.'").value = '.$_GET["child_number$i"].';';
					for($j=1;$j<=$y;$j++){
						$s='age_number_'.$i.'_'.$j;
						$age=$_GET[$s];
						echo 'document.getElementById("agenf_" + '.$i.' + "_" + '.$j.').style.display = "block";';
						echo 'document.getElementById("age_number_'.$i.'_'.$j.'").value = "'.$age.'";';
					}
			
				}
			}   
		}

		?>
</script>