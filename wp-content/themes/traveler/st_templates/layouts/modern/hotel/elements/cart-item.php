<?php
if(isset($item_id) and $item_id):
    $item               = STCart::find_item($item_id); 
    $hotel              = $item_id;
    $room_num_search    = $item['data']['room_num_search'];
    $check_in           = $item['data']['check_in'];
    $check_out          = $item['data']['check_out'];
    $date_diff          = STDate::dateDiff($check_in,$check_out);
    $extra_prices       = 0;
    $supp_single        = 0;
    
?>
<div class="service-section">
    <div class="service-left">
        <h4 class="title"><a href="<?php echo get_permalink($hotel)?>"><?php echo get_the_title($hotel)?></a></h4>
        <?php
        $address = get_post_meta( $item_id, 'address', true);
        if( $address ):
            ?>
            <p class="address"><?php echo TravelHelper::getNewIcon('Ico_maps', '#666666', '15px', '15px', true); ?><?php echo esc_html($address); ?> </p>
            <?php
        endif;
        ?>
    </div>
    <div class="service-right">
        <?php echo get_the_post_thumbnail($hotel,array(110,110,'bfi_thumb'=>true), array('alt' => TravelHelper::get_alt_image(get_post_thumbnail_id($hotel )), 'class' => 'img-responsive'));?>
    </div>
</div>

<div class="info-section">
    <ul>
	    <li class="ad-info-">
	    <ul>
	        <li><span class="label"><?php echo __('Nombre de nuits', ST_TEXTDOMAIN); ?></span><span class="value">
                <?php
                    if($date_diff>1){
                        printf(__('%d Nuits', ST_TEXTDOMAIN), $date_diff);
                    }else{
                        printf(__('%d Nuit', ST_TEXTDOMAIN), $date_diff);
                    }
                ?>
	        </span>
	        </li>
	        <li>
                <span class="label"><?php echo __('Nombre de chambres', ST_TEXTDOMAIN); ?></span><span class="value"><?php echo esc_html($item['number']).__(' Chambre(s)', ST_TEXTDOMAIN);?></span>
            </li>
	        <li>
            <span class="label">
                <?php echo __('Date', ST_TEXTDOMAIN); ?>
            </span>
            <span class="value">
                <?php echo date_i18n( TravelHelper::getDateFormat(), strtotime( $check_in ) ); ?>
                -
                <?php echo date_i18n( TravelHelper::getDateFormat(), strtotime( $check_out ) ); ?>
                <?php
                    $start = date( TravelHelper::getDateFormat(), strtotime( $check_in ) );
                    $end   = date( TravelHelper::getDateFormat(), strtotime( $check_out ) );
                    $date  = date( 'd/m/Y h:i a', strtotime( $check_in ) ) . '-' . date( 'd/m/Y h:i a', strtotime( $check_out ) );
                    $args  = [
                        'start' => $start,
                        'end'   => $end,
                        'date'  => $date
                    ];
                ?>
                <a class="st-link" style="font-size: 12px;" href="<?php echo add_query_arg( $args, get_the_permalink( $item_id ) ); ?>"><?php echo __( 'Modifier', ST_TEXTDOMAIN ); ?></a>
            </span>
        </li>
	</ul>
    </li>
    <hr style=" border-top: 3px solid #5191FA; ">
	<?php for ($i = 1; $i <= $room_num_search; $i++) :
            $room_id = $item['data']['result'][$i]['room_id'];
            $arrangement_id = $item['data']['result'][$i]['arrangement_id'];
            
            $result_adult_child         = (array) $item['data']['result'][$i]['result_adult_child'];
            $result_adult_child_calc    = (array) $item['data']['result'][$i]['result_adult_child_calc'];
            $adult_number               = isset($result_adult_child_calc['nb_adult']) ? $result_adult_child_calc['nb_adult'] : 0;
            $child_number               = isset($result_adult_child_calc['nb_children']) ? $result_adult_child_calc['nb_children'] : 0;
            $adult_child_number         =  + $adult_number + $child_number;
            $extras                     = isset($item['data']['result'][$i]['extra_price']) ? $item['data']['result'][$i]['extra_price'] : array();
            $extra_single               = isset($item['data']['result'][$i]['extra_single']) ? $item['data']['result'][$i]['extra_single'] : array();
            $discount_beds              = isset($item['data']['result'][$i]['discount_beds']) ? $item['data']['result'][$i]['discount_beds'] : array();
    ?>
        <li><span class="label"><?php echo __('Chambre n° ' .$i , ST_TEXTDOMAIN); ?></span><span class="value"><?php echo get_the_title($room_id)?></span></li>
        <li><span class="label"><?php echo __('Arragement ' , ST_TEXTDOMAIN); ?></span><span class="value"><?php echo get_term_by('id', $arrangement_id, 'hotel-accommodation')->name ?></span></li>
        <li>
            <ul>
                <?php 
                $adult_number = "adult_number_".$i;
                $child_number = "child_number_".$i;
                ?>

                <?php if($result_adult_child[$i]->$adult_number) {?>
                <li class="left-mar">
                    <span class="label"><?php echo __('Nombre d\'adultes', ST_TEXTDOMAIN); ?></span>
                    <span class="value"><?php echo esc_attr($result_adult_child[$i]->$adult_number)." adulte(s)"; ?></span>
                </li>
                <?php } ?>
                <?php if($result_adult_child[$i]->$child_number) {?>
                    <li class="left-mar">
                        <span class="label"><?php echo __('Nombre d\'enfants', ST_TEXTDOMAIN); ?></span>
                        <span class="value"><?php echo esc_attr($result_adult_child[$i]->$child_number)." enfant(s)"; ?></span>
                    </li>
                <?php } ?>	
            </ul>
        </li>
        	 
	  <?php if(isset($extras) && is_array($extras) && count($extras)):  ?>
            <li>
                <span class="label"><?php echo __('Supplément(s)', ST_TEXTDOMAIN); ?></span>
                    <span class="value">
                        <span class="pull-right-">
                            <?php echo esc_html($extras['title']);?>
                        </span> <br/>
                    </span>
            </li>
            <?php
                // prices already calculated
                $extra_price = (float) $extras['price'];
                $extra_prices += $extra_price;
            ?>
			<li>
                <span class="label"><?php echo __('Prix Supp.', ST_TEXTDOMAIN); ?></span>
                <span class="value"><?php echo TravelHelper::format_money($extra_price); ?></span>
            </li>
     <?php endif; ?>
     <?php if(isset($extra_single) && is_array($extra_single) && count($extra_single)):  ?>
            <li>
                <span class="label"><?php echo __('Supplément(s)', ST_TEXTDOMAIN); ?></span>
                    <span class="value">
                        <span class="pull-right-">
                            <?php echo esc_html($extra_single['title']); ?>
                        </span> <br/>
                    </span>
            </li>
            <?php
                // prices already calculated
                $supp_single = (float) $extra_single['price'];
            ?>
			<li>
                <span class="label"><?php echo __('Prix Supp.', ST_TEXTDOMAIN); ?></span>
                <span class="value"><?php echo TravelHelper::format_money($supp_single); ?></span>
            </li>
     <?php endif; ?>
     <?php if(isset($discount_beds) && is_array($discount_beds) && count($discount_beds)):  ?>
            <li>
                <span class="label"><?php echo __('Réduction', ST_TEXTDOMAIN); ?></span>
                    <span class="value">
                        <span class="pull-right-">
                            <?php echo esc_html($discount_beds['title']); ?>
                        </span> <br/>
                    </span>
            </li>
            <?php
                // prices already calculated
                $discount_beds_price = (float) $discount_beds['price'];
            ?>
			<li>
                <span class="label"><?php echo __('Prix réduction.', ST_TEXTDOMAIN); ?></span>
                <span class="value"><?php echo $discount_beds_price . " %"; ?></span>
            </li>
     <?php endif; ?>
    <li class="left-mar price-cart">
            <span class="label"><?php echo __('Prix', ST_TEXTDOMAIN); ?></span>
            <span class="value"><?php echo TravelHelper::format_money($item['data']['result'][$i]['sale_price']); ?></span>
    </li>
     <hr>
      <?php
        if(isset($item['data']['deposit_money'])):
            $deposit = $item['data']['deposit_money'];
            if(!empty($deposit['type']) and !empty($deposit['amount'])){
            ?>
            <li>
                <span class="label"><?php printf(__('Deposit %s',ST_TEXTDOMAIN),$deposit['type']) ?></span>
                <span class="value pull-right">
                    <?php
                    switch($deposit['type']){
                        case "percent":
                            echo esc_html($deposit['amount']).' %';
                            break;
                        case "amount":
                            echo TravelHelper::format_money($deposit['amount']);
                            break;
                    }
                    ?>
                </span>
            </li>
        <?php } endif; ?>
        <?php endfor; // end for ?>
    </ul>
</div>

<div class="coupon-section">
    <h5><?php echo __('Code coupon', ST_TEXTDOMAIN); ?></h5>

    <form method="post" action="<?php the_permalink() ?>">
        <?php if (isset(STCart::$coupon_error['status'])): ?>
            <div
                class="alert alert-<?php echo STCart::$coupon_error['status'] ? 'success' : 'danger'; ?>">
                <p>
                    <?php echo STCart::$coupon_error['message'] ?>
                </p>
            </div>
        <?php endif; ?>

        <div class="form-group">
            <?php $code = STInput::post('coupon_code') ? STInput::post('coupon_code') : STCart::get_coupon_code();?>
            <input id="field-coupon_code" value="<?php echo esc_attr($code ); ?>" type="text" name="coupon_code" />
            <input type="hidden" name="st_action" value="apply_coupon">
            <?php if(st()->get_option('use_woocommerce_for_booking','off') == 'off' && st()->get_option('booking_modal','off') == 'on' ){ ?>
                <input type="hidden" name="action" value="ajax_apply_coupon">
                <button type="submit" class="btn btn-primary add-coupon-ajax"><?php echo __('Appliquer', ST_TEXTDOMAIN); ?></button>
                <div class="alert alert-danger hidden"></div>
            <?php }else{ ?>
                <button type="submit" class="btn btn-primary"><?php echo __('Appliquer', ST_TEXTDOMAIN); ?></button>
            <?php } ?>
        </div>
    </form>
</div>
<div class="total-section">
    <?php
    $price          = floatval(get_post_meta($room_id, 'price', true));
    $number_room    = intval($item['number']);
    $numberday      = STDate::dateDiff($check_in, $check_out);
    $sale_price     = $item['data']['ori_price'] ;
    $sale_price    += $extra_prices;
    $sale_price    += $supp_single;
	$discount       = $item['data']['discount_rate'] ? $item['data']['discount_rate'] : 0;
    $price_coupon   = floatval(STCart::get_coupon_amount());
    $price_with_tax = STPrice::getPriceWithTax($sale_price);
    $price_with_tax -= $price_coupon;
    $price_with_tax = $sale_price - ($sale_price * ( $discount / 100 ));
    // $price_with_tax = $sale_price;
    ?>
    <ul>
        <li><span class="label"><?php echo __('Sous-total', ST_TEXTDOMAIN); ?></span><span class="value"><?php echo TravelHelper::format_money($sale_price); ?></span></li>
        <li><span class="label"><?php echo __('Taxes', ST_TEXTDOMAIN); ?></span><span class="value"><?php echo STPrice::getTax().' %'; ?></span></li>
	<li class="remises">
        <span class="label"><?php echo __('Remises', ST_TEXTDOMAIN); ?></span>
        <span class="value"><?php echo $discount.' %'; ?></span></li>
        <?php if (STCart::use_coupon()):
            if($price_coupon < 0) $price_coupon = 0;
            ?>
            <li>
                <span class="label text-left">
                    <?php printf(st_get_language('coupon_key'), STCart::get_coupon_code()) ?> <br/>
                    <?php if(st()->get_option('use_woocommerce_for_booking','off') == 'off' && st()->get_option('booking_modal','off') == 'on' ){ ?>
                        <a href="javascript: void(0);" title="" class="ajax-remove-coupon" data-coupon="<?php echo STCart::get_coupon_code(); ?>"><small class='text-color'>(<?php st_the_language('Remove coupon') ?> )</a>
                    <?php }else{ ?>
                        <a href="<?php echo st_get_link_with_search(get_permalink(), array('remove_coupon'), array('remove_coupon' => STCart::get_coupon_code())) ?>"
                           class="danger"><small class='text-color'>(<?php st_the_language('Remove coupon') ?> )</small></a>
                    <?php } ?>
                </span>
                <span class="value">
                        - <?php echo TravelHelper::format_money( $price_coupon ) ?>
                </span>
            </li>
        <?php endif; ?>

        <?php
        if(isset($item['data']['deposit_money']) && count($item['data']['deposit_money']) && floatval($item['data']['deposit_money']['amount']) > 0):

            $deposit      = $item['data']['deposit_money'];
            $deposit_price = $price_with_tax;

            if($deposit['type'] == 'percent'){
                $de_price = floatval($deposit['amount']);
                $deposit_price = $deposit_price * ($de_price /100);
            }elseif($deposit['type'] == 'amount'){
                $de_price = floatval($deposit['amount']);
                $deposit_price = $de_price;
            }
            ?>
            <li>
                <span class="label"><?php echo __('Total', ST_TEXTDOMAIN); ?></span>
                <span class="value"><?php echo TravelHelper::format_money($price_with_tax); ?></span>
            </li>
            <li>
                <span class="label"><?php echo __('Deposit', ST_TEXTDOMAIN); ?></span>
                <span class="value">
                    <?php echo TravelHelper::format_money($deposit_price); ?>
                </span>
            </li>
            <?php
            $total_price = 0;
            if(isset($item['data']['deposit_money']) && floatval($item['data']['deposit_money']['amount']) > 0){
                $total_price = $deposit_price;
            }else{
                $total_price = $price_with_tax;
            }
            ?>
            <?php if(!empty($item['data']['booking_fee_price'])){
            $total_price = $total_price + $item['data']['booking_fee_price'];
            ?>
            <li>
                <span class="label"><?php echo __('Fee', ST_TEXTDOMAIN); ?></span>
                <span class="value"><?php echo TravelHelper::format_money($item['data']['booking_fee_price']); ?></span>
            </li>
            <?php } ?>
            <li class="payment-amount">
                <span class="label"><?php echo __('Total', ST_TEXTDOMAIN); ?></span>
                <span class="value">
                        <?php echo TravelHelper::format_money($total_price); ?>
                </span>
            </li>

        <?php else: ?>
            <?php if(!empty($item['data']['booking_fee_price'])){
                $price_with_tax = $price_with_tax + $item['data']['booking_fee_price'];
                ?>
                <li>
                    <span class="label"><?php echo __('Fee', ST_TEXTDOMAIN); ?></span>
                    <span class="value"><?php echo TravelHelper::format_money($item['data']['booking_fee_price']); ?></span>
                </li>
            <?php } ?>
            <li class="payment-amount">
                <span class="label"><?php echo __('Total', ST_TEXTDOMAIN); ?></span>
                <span class="value"><?php echo TravelHelper::format_money($price_with_tax); ?></span>
            </li>
        <?php endif; ?>
    </ul>
</div>
<?php
endif;
?>
